// $(document).ready(function() {

// 	$('.menu-trigger').click(function(){

// 		$('body').toggleClass('nav-menu-open');

// 	});

// 	$(".home .cover-heading").typed({
// 		strings: ["Ectem aceperro ^1000 eat aut ent erum", "Ectem aceperro eat aut ent erum inimus eaquat utet voluptati"],
// 		typeSpeed: 0
// 	});

// 	$('.case-nav-projects-item a').click(function(){

// 	$('.case-praktikertjanst').removeClass('visible').addClass('animated bounceOutLeft hidden');
// 	$('.case-swedavia').addClass('visible animated bounceInLeft').removeClass('hidden');

// 	return false;

// 	});  

//     $('.case-nav-projects-item a').click(function(){
		
// 		var case_name = $(this).attr('case-name');

// 		$('.case-' + case_name)
// 			.addClass('fadeInLeftBig visible')
// 			.removeClass('fadeOutRightBig hidden');

		
// 		$('.case').not('.case-' + case_name)
// 			.addClass('fadeOutRightBig hidden')
// 			.removeClass('fadeInLeftBig visible');

//      	// console.log('.case-' + case_name);

//      return false;

//    });

// });

$(document).ready(function() {

	$('.menu-trigger').click(function(){
		if($(this).hasClass('menu-close')) {
			$(this).removeClass('menu-close');
			$('body').removeClass('nav-menu-open');
			$('#navigation').removeClass('nav-menu-items-down');
		} else {
			$(this).addClass('menu-close');
			$('body').addClass('nav-menu-open').removeClass('nav-menu-up');
			$('#navigation').removeClass('nav-menu-items-up');
			$('#navigation').addClass('nav-menu-items-down');
		}
	});

	$('.main-nav .main-navigation > li a, .logo').click(function(e){

		$('#navigation').addClass('nav-menu-items-up');
		$('body').addClass('nav-menu-up');
		   setTimeout(function() {
		        $('body').removeClass('nav-menu-open');
		        $('#navigation').removeClass('nav-menu-items-down');
		        $('.menu-trigger').removeClass('menu-close');

				//var link = $(e.target).attr('href');
				//var hash = link.substring(link.indexOf('#'));

				//location.hash = hash;

		    }, 1000)

	});

	function parallaxIt() {
	  var $fwindow = $(window);
	 
	  $('[data-type="content"]').each(function (index, e) {
	    var scrollTop = $fwindow.scrollTop();
	    var $contentObj = $(this);
	 
	    $fwindow.on('scroll resize', function (){
	      scrollTop = $fwindow.scrollTop();
	 
	      $contentObj.css('top', ($contentObj.height() * index) - scrollTop);
	    });
	  });
	 
	  $('[data-type="background"]').each(function(){
	    var $backgroundObj = $(this);
	 
	    $fwindow.on('scroll resize', function() {
	      var yPos = - ($fwindow.scrollTop() / $backgroundObj.data('speed')); 
	      console.log(yPos)
	      var coords = '50% '+ yPos + 'px';
	 
	      // Move the background
	      $backgroundObj.css({ backgroundPosition: coords });
	    }); 
	  }); 
	   
	  $fwindow.trigger('scroll');
	};
	 
	parallaxIt();


    $('.next').click(function() {
        var $next_section = $(this).parent().next();

		$('html, body').animate({
			scrollTop: $($next_section).offset().top
		}, 800, function(){
			// when done, add hash to url
			// (default click behaviour)
			// window.location.hash = hash;
		});

    });

});