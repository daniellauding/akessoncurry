$(document).ready(function() {

	$('.menu-trigger').click(function(){

		$('body').toggleClass('nav-menu-open');

	});

	$(".home .cover-heading").typed({
		strings: ["Ectem aceperro ^1000 eat aut ent erum", "Ectem aceperro eat aut ent erum inimus eaquat utet voluptati"],
		typeSpeed: 0
	});

	$('.case-nav-projects-item a').click(function(){

	$('.case-praktikertjanst').removeClass('visible').addClass('animated bounceOutLeft hidden');
	$('.case-swedavia').addClass('visible animated bounceInLeft').removeClass('hidden');

	return false;

	});  

    $('.case-nav-projects-item a').click(function(){
		
		var case_name = $(this).attr('case-name');

		$('.case-' + case_name)
			.addClass('fadeInLeftBig visible')
			.removeClass('fadeOutRightBig hidden');

		
		$('.case').not('.case-' + case_name)
			.addClass('fadeOutRightBig hidden')
			.removeClass('fadeInLeftBig visible');

     	// console.log('.case-' + case_name);

     return false;

   });

});